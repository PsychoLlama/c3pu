import * as http from 'http';
import config from './config';
import routes from './routes';

const server = http.createServer(routes);

server.listen(config.port, () => {
  console.log(server.address());
});
