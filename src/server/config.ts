// Normally these values would pull from `process.env` with defaults, but in
// this case I'm prioritizing consistency over configurability. Dev is my
// production.
export default { port: 8081 };
