import os from 'os';
import assert from 'assert';

/**
 * A cross-platform alternative to `os.loadavg()` with arbitrary time
 * precision. Due to platform limitations, this cannot represent cases where
 * the CPU is overloaded and has a pending process queue. It maxes out at
 * 100%.
 *
 * REVIEWERS: The instructions suggested using `os.loadavg()` and alluded to
 * packages for windows support. Of most packages I've sampled, they just use
 * available data from `os.cpus()`, which appears to be in line with monitoring
 * tools like `glances` and `bottom`. (The other method was really sketchy.)
 *
 * Because this can't represent cases where CPU usage >100%, I've chosen to use
 * this exclusively as a backup for Windows. I hope that's okay.
 */
export class CpuUsageObserver {
  private config: Config;
  private interval: null | ReturnType<typeof setInterval> = null;

  // A list of busy/idle time CPU samples, totaled over all cores. Each
  // timestamp is cumulative since boot. The oldest value is reserved solely
  // to determine how much to subtract from subsequent samples so they only
  // capture recent data.
  private samples: Array<Sample> = [];

  constructor(config: Config = { sampleRate: 6_000, sampleSize: 10 }) {
    this.config = config;
  }

  /** Begin observing CPU usage. */
  start() {
    this.takeSample();
    this.interval = setInterval(this.takeSample, this.config.sampleRate);
  }

  /** Stop observing CPU usage and clear the observer. */
  stop() {
    this.samples = [];

    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  /**
   * Returns the average CPU load across all cores for the configured time
   * scale.
   */
  getLoadAverage() {
    assert(
      this.samples.length > 0,
      'You must call `start()` before reading values',
    );

    if (this.samples.length === 1) {
      return this.getApproximateLoadAverage();
    }

    // Each sample only grows in time spent idle/busy. We need to compare it
    // against the sample just before to figure out what changed.
    const diffs = this.samples.reduce((diffs: Array<Sample>, sample, index) => {
      const lastSample = this.samples[index - 1];

      return diffs.concat({
        timeSpentBusy: sample.timeSpentBusy - (lastSample?.timeSpentBusy ?? 0),
        timeSpentIdle: sample.timeSpentIdle - (lastSample?.timeSpentIdle ?? 0),
      });
    }, []);

    // The first item is only used to determine an offset. It is excluded from
    // the results.
    const normalizedSamples = diffs.slice(1);

    const total: Sample = {
      timeSpentBusy: this.getTimeSpentBusy(normalizedSamples),
      timeSpentIdle: this.getTimeSpentIdle(normalizedSamples),
    };

    return total.timeSpentBusy / (total.timeSpentBusy + total.timeSpentIdle);
  }

  /**
   * Gets the average load since boot. This is only used when we don't have
   * enough samples to determine the recent load.
   */
  private getApproximateLoadAverage() {
    const [sample] = this.samples;

    const totalTime = sample.timeSpentBusy + sample.timeSpentIdle;
    return sample.timeSpentBusy / totalTime;
  }

  private takeSample = () => {
    const cores: Array<Sample> = os.cpus().map(({ times: t }) => ({
      timeSpentBusy: t.user + t.nice + t.sys + t.irq,
      timeSpentIdle: t.idle,
    }));

    this.samples.push({
      timeSpentBusy: this.getTimeSpentBusy(cores),
      timeSpentIdle: this.getTimeSpentIdle(cores),
    });

    // Remember, the oldest value is only kept to know how much to subtract
    // from the more recent samples.
    this.samples = this.samples.slice(-this.config.sampleSize - 1);
  };

  private getTimeSpentBusy(samples: Array<Sample>) {
    return samples.reduce((sum, sample) => sum + sample.timeSpentBusy, 0);
  }

  private getTimeSpentIdle(samples: Array<Sample>) {
    return samples.reduce((sum, sample) => sum + sample.timeSpentIdle, 0);
  }
}

interface Config {
  /**
   * How frequently to capture new values. This affects how quickly the value
   * is updated.
   */
  sampleRate: number;

  /** The number of samples to average against. */
  sampleSize: number;
}

interface Sample {
  timeSpentBusy: number;
  timeSpentIdle: number;
}

export default new CpuUsageObserver();
