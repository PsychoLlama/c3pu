import os, { CpuInfo } from 'os';
import { CpuUsageObserver } from '../cpu-usage';

jest.useFakeTimers('modern');

const cpus = jest.spyOn(os, 'cpus');

describe('CpuUsageObserver', () => {
  function createCpu(overrides?: Partial<CpuInfo['times']>): CpuInfo {
    return {
      model: 'Difference Engine #2',
      speed: NaN,
      times: {
        user: 0,
        nice: 0,
        sys: 0,
        idle: 0,
        irq: 0,
        ...overrides,
      },
    };
  }

  beforeEach(() => {
    jest.clearAllMocks();
    jest.clearAllTimers();
    cpus.mockReturnValue([createCpu()]);
  });

  it('initializes with the system average since boot', () => {
    const observer = new CpuUsageObserver();

    cpus.mockReturnValue([
      createCpu({ sys: 90, idle: 5 }),
      createCpu({ sys: 90, idle: 15 }),
    ]);

    observer.start();

    expect(observer.getLoadAverage()).toBe(0.9);
  });

  it('discards the system boot average once recent data is available', () => {
    const observer = new CpuUsageObserver();

    cpus.mockReturnValue([createCpu({ sys: 10, idle: 0 })]);
    observer.start();

    cpus.mockReturnValue([createCpu({ nice: 15, idle: 5 })]);
    jest.runOnlyPendingTimers();

    cpus.mockReturnValue([createCpu({ nice: 15, idle: 15 })]);
    jest.runOnlyPendingTimers();

    // total=20, busy=5
    expect(observer.getLoadAverage()).toBe(0.25);
  });

  it('drops old values as the sample period moves forward', () => {
    const observer = new CpuUsageObserver({ sampleSize: 1, sampleRate: 5 });

    cpus.mockReturnValue([createCpu({ user: 5, idle: 15 })]);
    observer.start();

    expect(observer.getLoadAverage()).toBe(0.25);

    cpus.mockReturnValue([createCpu({ user: 20, idle: 20 })]);
    jest.runOnlyPendingTimers();

    // The next sample will replace the old one (sampleSize=1).
    cpus.mockReturnValue([createCpu({ user: 25, idle: 35 })]);
    jest.runOnlyPendingTimers();

    // total=20, busy=5
    expect(observer.getLoadAverage()).toBe(0.25);
  });

  it('throws if you try to observe the value after teardown', () => {
    const observer = new CpuUsageObserver();
    observer.start();
    observer.stop();

    const fail = () => observer.getLoadAverage();

    expect(fail).toThrow(/before reading values/i);
  });

  it('allows reuse of the observer if you restart', () => {
    const observer = new CpuUsageObserver();

    cpus.mockReturnValue([createCpu({ idle: 20 })]);
    observer.start();
    observer.stop();

    // Old values should be cleared.
    cpus.mockReturnValue([createCpu({ nice: 20 })]);
    observer.start();
    expect(observer.getLoadAverage()).toBe(1);

    cpus.mockReturnValue([createCpu({ nice: 40, idle: 20 })]);
    jest.runOnlyPendingTimers();
    expect(observer.getLoadAverage()).toBe(0.5);
  });
});
