import request from 'supertest';
import mockedOs from 'os';
import app from '../../routes';
import observer from '../../observers/cpu-usage';

jest.mock('os');
jest.mock('../../observers/cpu-usage');

const os: jest.Mocked<typeof mockedOs> = mockedOs as any;
const getLoadAverage = jest.spyOn(observer, 'getLoadAverage');

describe('/api/v1', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    os.platform.mockReturnValue('linux');
    os.loadavg.mockReturnValue([2, 4, 1]);
    getLoadAverage.mockReturnValue(0.5);
    os.cpus.mockReturnValue([
      {
        model: 'Toddler with an Abacus',
        speed: 1,
        times: {
          user: 4,
          nice: 0,
          sys: 3,
          idle: 2,
          irq: 9001,
        },
      },
    ]);
  });

  describe('/status', () => {
    it('returns healthy', async () => {
      await request(app).get('/api/v1/status').expect(200).send();
    });
  });

  describe('/metrics/cpu', () => {
    it('returns basic metrics about the CPUs', async () => {
      const response = await request(app)
        .get('/api/v1/metrics/cpu')
        .expect(200)
        .send();

      expect(response.body).toMatchObject({
        loadAverage: 2,
      });
    });

    it('calculates the average load over the number of cores', async () => {
      const [cpu] = os.cpus();
      os.cpus.mockReturnValue(Array(4).fill(cpu));

      const response = await request(app)
        .get('/api/v1/metrics/cpu')
        .expect(200)
        .send();

      expect(response.body).toMatchObject({
        loadAverage: 0.5,
      });
    });

    it('uses the CPU observer source on Windows platforms', async () => {
      os.platform.mockReturnValue('win32');
      getLoadAverage.mockReturnValue(0.99);

      const response = await request(app)
        .get('/api/v1/metrics/cpu')
        .expect(200)
        .send();

      expect(response.body).toMatchObject({
        loadAverage: 0.99,
      });
    });
  });
});
