import { Router } from 'express';
import os from 'os';
import cpuObserver from '../observers/cpu-usage';

cpuObserver.start();

const api = Router();

// This endpoint is just used for sanity checks. It always returns 200 OK.
api.get('/status', (_req, res) => {
  res.status(200).json({ status: 'online' });
});

// A better metrics endpoint might send `cpu.times` for each CPU allowing more
// interesting reporting on the frontend. But that's out of scope for this
// iteration.
api.get('/metrics/cpu', (_req, res) => {
  const [loadAverage] = os.loadavg();
  const cpuCount = os.cpus().length;

  res.json({
    loadAverage:
      os.platform() === 'win32'
        ? cpuObserver.getLoadAverage()
        : loadAverage / cpuCount,
  });
});

export default api;
