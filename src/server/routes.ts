import express from 'express';
import createLogger from 'debug';
import path from 'path';
import apiRoutes from './api';

const log = createLogger('c3pu:server');
const routes = express();

// Static content is resolved relative to `dist/`, not `src/`.
const staticContentPath = path.join(__dirname, '../public');
routes.use('/app', express.static(staticContentPath));

// Log all requests.
routes.use(function loggingMiddleware(req, _res, next) {
  log(req.method, req.url);
  next();
});

routes.use('/api/v1', apiRoutes);

export default routes;
