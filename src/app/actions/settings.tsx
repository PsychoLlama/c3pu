import { createAction } from 'retreon';
import { State } from '../reducers/initial-state';

export const updateSampleRate = createAction<{
  metric: keyof State['metrics'];
  value: number;
}>('settings/update-sample-rate');

export const updateSamplePeriod = createAction<{
  metric: keyof State['metrics'];
  value: number;
}>('settings/update-sample-period');

export const updateUpperThreshold = createAction<{
  metric: keyof State['metrics'];
  value: number;
}>('settings/update-upper-threshold');

export const updateEscalationDelay = createAction<{
  metric: keyof State['metrics'];
  value: number;
}>('settings/update-escalation-delay');

export const updateCooldownDelay = createAction<{
  metric: keyof State['metrics'];
  value: number;
}>('settings/update-cooldown-delay');
