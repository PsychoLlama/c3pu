import { createAction } from 'retreon';
import * as effects from '../effects';

export const getCpuMetrics = createAction.async(
  'collectors/get-cpu-metrics',
  effects.collectors.getCpuMetrics,
);
