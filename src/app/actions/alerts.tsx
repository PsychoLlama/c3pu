import { createAction } from 'retreon';

export const requestPermission = createAction.async(
  'alerts/request-permission',
  Notification.requestPermission,
);
