import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { v4 as uuid } from 'uuid';
import { NumberInput } from './core';
import { State } from '../reducers/initial-state';
import * as actions from '../actions';
import * as css from '../utils/css';

// Metric ID is hard-coded. If another metric is added, these settings could
// be rendered only after clicking a hypothetical edit button on the
// dashboard.
const DEFAULT_METRIC = 'cpuLoad' as const;

export class Settings extends React.Component<Props> {
  private ids = {
    sampleRate: uuid(),
    samplePeriod: uuid(),
    upperThreshold: uuid(),
    timeToEscalation: uuid(),
    timeToCooldown: uuid(),
  };

  render() {
    const {
      sampleRate,
      samplePeriod,
      upperThreshold,
      timeToEscalation,
      timeToCooldown,
    } = this.props;

    if (process.env.NODE_ENV === 'production') {
      return null;
    }

    return (
      <Sidebar>
        <ControlsGroup>
          <Title>Development Settings</Title>

          <Label htmlFor={this.ids.sampleRate}>Poll Interval</Label>
          <NumberInput
            data-test="sample-rate-input"
            id={this.ids.sampleRate}
            min={5}
            max={60}
            value={sampleRate}
            onChange={this.updateSampleRate}
          />

          <Label htmlFor={this.ids.samplePeriod}>Sample Period</Label>
          <NumberInput
            data-test="sample-period-input"
            id={this.ids.samplePeriod}
            min={60}
            max={60 * 60}
            value={samplePeriod}
            onChange={this.updateSamplePeriod}
          />

          <Subtitle>Alerts</Subtitle>

          <Label htmlFor={this.ids.upperThreshold}>Upper Threshold</Label>
          <NumberInput
            data-test="upper-threshold-input"
            id={this.ids.upperThreshold}
            min={1}
            max={500}
            value={upperThreshold}
            onChange={this.updateUpperThreshold}
          />

          <Label htmlFor={this.ids.timeToEscalation}>Escalation Delay</Label>
          <NumberInput
            data-test="escalation-delay-input"
            id={this.ids.timeToEscalation}
            min={0}
            max={60 * 60}
            value={timeToEscalation}
            onChange={this.updateEscalationDelay}
          />

          <Label htmlFor={this.ids.timeToCooldown}>Cooldown Delay</Label>
          <NumberInput
            data-test="cooldown-delay-input"
            id={this.ids.timeToCooldown}
            min={0}
            max={60 * 60}
            value={timeToCooldown}
            onChange={this.updateCooldownDelay}
          />

          <Footnote>All units are measured in seconds.</Footnote>
        </ControlsGroup>
      </Sidebar>
    );
  }

  private bindToChangeHandler = (
    update: typeof actions.settings.updateSampleRate,
  ) => {
    return function updateValue(value: number) {
      update({ metric: DEFAULT_METRIC, value });
    };
  };

  private updateSampleRate = this.bindToChangeHandler(
    this.props.updateSampleRate,
  );

  private updateSamplePeriod = this.bindToChangeHandler(
    this.props.updateSamplePeriod,
  );

  private updateUpperThreshold = this.bindToChangeHandler(
    this.props.updateUpperThreshold,
  );

  private updateEscalationDelay = this.bindToChangeHandler(
    this.props.updateEscalationDelay,
  );

  private updateCooldownDelay = this.bindToChangeHandler(
    this.props.updateCooldownDelay,
  );
}

interface Props {
  updateSampleRate: typeof actions.settings.updateSampleRate;
  updateSamplePeriod: typeof actions.settings.updateSamplePeriod;
  updateUpperThreshold: typeof actions.settings.updateUpperThreshold;
  updateEscalationDelay: typeof actions.settings.updateEscalationDelay;
  updateCooldownDelay: typeof actions.settings.updateCooldownDelay;
  sampleRate: number;
  samplePeriod: number;
  upperThreshold: number;
  timeToEscalation: number;
  timeToCooldown: number;
}

const Sidebar = styled.aside.attrs({ role: 'complementary' })`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 20rem;
  background-color: ${css.color('dark')};
  box-shadow: 0 2px 4px ${css.color('dark')};
  padding: 1rem;
`;

const Title = styled.h2`
  margin: 0;
`;

const Subtitle = styled.h3`
  margin: 0;
  margin-top: 2rem;
`;

const ControlsGroup = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.label`
  width: max-content;
  margin-top: 1rem;
  margin-bottom: 0.5rem;
`;

const Footnote = styled.small`
  font-style: italic;
  margin-top: 1rem;
`;

export function mapStateToProps(state: State) {
  const metric = state.metrics[DEFAULT_METRIC];
  const { timeToEscalation, timeToCooldown } = metric.monitoring;

  return {
    sampleRate: metric.sampleRate,
    samplePeriod: metric.samplePeriod,
    upperThreshold: metric.monitoring.threshold.upperBound,
    timeToEscalation,
    timeToCooldown,
  };
}

const mapDispatchToProps = {
  updateSampleRate: actions.settings.updateSampleRate,
  updateSamplePeriod: actions.settings.updateSamplePeriod,
  updateUpperThreshold: actions.settings.updateUpperThreshold,
  updateEscalationDelay: actions.settings.updateEscalationDelay,
  updateCooldownDelay: actions.settings.updateCooldownDelay,
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
