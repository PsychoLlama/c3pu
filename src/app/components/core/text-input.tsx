import styled from 'styled-components';
import FreeformInput from 'freeform-input';
import * as css from '../../utils/css';

const TextInput = styled(FreeformInput)`
  appearance: none;
  padding: 0.5rem;
  border-radius: 3px;
  border: 1px solid ${css.color('foreground')};
  background-color: ${css.color('background')};
  color: ${css.color('foreground')};

  ::placeholder {
    font-style: italic;
  }

  :hover,
  :focus {
    border-color: ${css.color('primary')};
    outline: none;
  }
`;

export default TextInput;
