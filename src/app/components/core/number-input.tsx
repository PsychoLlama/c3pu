import React from 'react';
import styled from 'styled-components';
import TextInput from './text-input';

/**
 * This is a typical input[type="text"] component, but with input validation
 * and value clamping.
 */
export default class NumberInput extends React.Component<Props> {
  render() {
    return <Input {...this.props} onChange={this.forwardChange} />;
  }

  forwardChange = (input: number | string) => {
    const { min, max } = this.props;
    const value = Number(input);

    // The native input allows arbitrary text.
    if (isNaN(value)) return;

    const clamped = Math.max(Math.min(value, max), min);

    // No change detected.
    if (clamped === this.props.value) return;

    this.props.onChange(clamped);
  };
}

interface Props
  extends Omit<React.ComponentProps<typeof Input>, 'onChange' | 'value'> {
  onChange(value: number): unknown;
  value: number;
}

const Input = styled(TextInput).attrs({ type: 'number' })`
  appearance: textfield;

  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    appearance: none;
    margin: 0;
  }
`;
