import styled from 'styled-components';

/**
 * A button with all user agent styles removed. Because this is a toy app,
 * there's no notion of a "standard" button, otherwise the styling might go
 * here instead.
 */
const Button = styled.button`
  appearance: none;
  background-color: transparent;
  color: inherit;
  padding: 0;
  border: none;
  cursor: pointer;

  :hover:not(:disabled),
  :focus:not(:disabled) {
    filter: brightness(80%);
  }

  :active {
    filter: brightness(70%);
  }

  :disabled {
    filter: grayscale(100%);
    cursor: not-allowed;
  }
`;

export default Button;
