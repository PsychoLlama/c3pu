import React from 'react';
import { shallow } from 'enzyme';
import Button from '../button';

describe('Button', () => {
  it('renders', () => {
    const setup = () => shallow(<Button />);

    expect(setup).not.toThrow();
  });
});
