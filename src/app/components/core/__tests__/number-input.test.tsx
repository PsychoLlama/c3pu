import React from 'react';
import { shallow } from 'enzyme';
import NumberInput from '../number-input';

describe('NumberInput', () => {
  function setup(
    overrides?: Partial<React.ComponentProps<typeof NumberInput>>,
  ) {
    const props = {
      onChange: jest.fn(),
      min: 5,
      max: 100,
      value: 10,
      ...overrides,
    };

    const output = shallow(<NumberInput {...props} />);

    return {
      output,
      props,
    };
  }

  it('ignores invalid inputs', () => {
    const { output, props } = setup();

    output.simulate('change', 'garbage');

    expect(props.onChange).not.toHaveBeenCalled();
  });

  it('clamps values to their min/max', () => {
    const { output, props } = setup({ min: 5, max: 15 });

    output.simulate('change', 0);
    expect(props.onChange).toHaveBeenCalledWith(5);

    output.simulate('change', 20);
    expect(props.onChange).toHaveBeenCalledWith(15);
  });

  it('does not emit a change event if nothing changed', () => {
    const { output, props } = setup({ value: 15, max: 15 });

    output.simulate('change', 20);

    expect(props.onChange).not.toHaveBeenCalled();
  });
});
