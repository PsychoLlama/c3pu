import React from 'react';
import { shallow } from 'enzyme';
import TextInput from '../text-input';

describe('TextInput', () => {
  it('renders', () => {
    const setup = () => shallow(<TextInput value="" />);

    expect(setup).not.toThrow();
  });
});
