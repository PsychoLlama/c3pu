import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { State } from '../reducers/initial-state';
import * as css from '../utils/css';
import { getIncidents, Incident } from '../selectors/incidents';

export class Incidents extends React.Component<Props> {
  render() {
    const { incidents } = this.props;

    return (
      <Table data-test="incidents">
        <Title>Incidents</Title>

        <thead>
          <tr>
            <TableHeader>Start Time</TableHeader>
            <TableHeader>Last Report</TableHeader>
            <TableHeader>Resolved</TableHeader>
          </tr>
        </thead>

        <tbody>
          {incidents.reverse().map(this.renderIncident)}

          {incidents.length === 0 && (
            <tr data-test="no-incidents">
              <WideCell>No Recent Incidents</WideCell>
            </tr>
          )}
        </tbody>
      </Table>
    );
  }

  private renderIncident = (incident: Incident) => {
    const formatter = new Intl.DateTimeFormat('en-US', {
      hour: 'numeric',
      minute: 'numeric',
    });

    const startDate = new Date(incident.firstNoticed * 1000);
    const lastReportDate = new Date(incident.lastReported * 1000);

    return (
      <tr key={incident.firstNoticed} data-test="incident">
        <TableCell>
          <time dateTime={startDate.toUTCString()} data-test="incident-time">
            {formatter.format(startDate)}
          </time>
        </TableCell>

        <TableCell data-test="incident-last-report">
          <time dateTime={lastReportDate.toUTCString()}>
            {formatter.format(lastReportDate)}
          </time>
        </TableCell>

        <TableCell data-test="incident-resolved">
          {incident.resolved ? 'Yes' : 'No'}
        </TableCell>
      </tr>
    );
  };
}

interface OwnProps {
  metric: keyof State['metrics'];
}

interface Props extends OwnProps {
  incidents: Array<Incident>;
}

const Table = styled.table`
  flex-grow: 1;
  border-collapse: collapse;
  overflow: auto;
  background-color: ${css.color('dark')};
  box-shadow: 0 2px 4px ${css.color('dark')};
`;

const Title = styled.caption`
  background-color: ${css.color('darkest')};
  font-size: 100%;
  font-weight: normal;
  padding: 1rem;
  margin: 0;
  text-align: left;
`;

const TableHeader = styled.th`
  border: 1px solid ${css.color('foreground')};
  background-color: ${css.color('foreground')};
  color: ${css.color('background')};
  padding: 0.5rem;
`;

const TableCell = styled.td`
  padding: 0.5rem;
  border: 1px solid ${css.color('darkest')};
`;

const WideCell = styled(TableCell).attrs({ colSpan: 3 })`
  text-align: center;
  padding: 1.5rem;
`;

export function mapStateToProps(state: State, { metric }: OwnProps) {
  return {
    incidents: getIncidents({ state, metric }),
  };
}

export default connect(mapStateToProps)(Incidents);
