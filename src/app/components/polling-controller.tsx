import React from 'react';

/**
 * It only makes sense to observe data sources that are actually rendered on
 * the page. This component dispatches a request periodically according to the
 * configured sample rate, binding the request cycle to a component lifetime.
 */
export default class PollingController extends React.Component<Props> {
  static defaultProps = {
    children: null,
  };

  private interval: null | ReturnType<typeof setInterval> = null;

  componentDidMount() {
    this.props.query();
    this.scheduleSampleIntervals();
  }

  componentWillUnmount() {
    this.clearSchedule();
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.sampleRate !== this.props.sampleRate) {
      this.clearSchedule();
      this.scheduleSampleIntervals();
    }
  }

  render() {
    return this.props.children;
  }

  private clearSchedule = () => {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  };

  private scheduleSampleIntervals = () => {
    this.interval = setInterval(
      () => this.props.query(),
      this.props.sampleRate * 1000,
    );
  };
}

interface Props {
  /** Something to actually fetch the data and load it in Redux. */
  query(): unknown;

  /** The preferred request interval (in seconds). */
  sampleRate: number;

  children?: React.ReactNode;
}
