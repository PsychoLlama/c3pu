import React from 'react';

/**
 * An element size observer powered by `ResizeObserver`. This is preferable to
 * other libraries like `react-sizeme` or `rc-resize-observer` because it
 * doesn't depend on deprecated React APIs.
 */
export class SizeObserver extends React.Component<Props> {
  private ref: null | HTMLDivElement = null;

  render() {
    return (
      <div data-test="size-observer" ref={this.bindRefToObserver}>
        {this.props.children}
      </div>
    );
  }

  private observer = new ResizeObserver(([entry]) => {
    this.props.onResize(entry.contentRect);
  });

  private bindRefToObserver = (ref: null | HTMLDivElement) => {
    if (ref) {
      this.observer.observe(ref);
    } else {
      this.observer.unobserve(this.ref!);
    }

    this.ref = ref;
  };
}

interface Props {
  children: React.ReactNode;
  onResize(container: BoundingBox): unknown;
}

export type BoundingBox = ResizeObserverEntry['contentRect'];

export default SizeObserver;
