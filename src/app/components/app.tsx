import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import TimeSeriesPlot from './time-series-plot';
import PollingController from './polling-controller';
import AlertController from './alert-controller';
import * as actions from '../actions';
import { State } from '../reducers/initial-state';
import Incidents from './incidents';
import Settings from './settings';
import AlertPermissionPrompt from './alert-permission-prompt';

export class App extends React.Component<Props> {
  render() {
    const { getCpuMetrics, sampleRate } = this.props;

    return (
      <Container>
        <Settings />

        <MainContent>
          <Dashboard>
            <PollingController query={getCpuMetrics} sampleRate={sampleRate}>
              <AlertController metric="cpuLoad" label="CPU">
                <TimeSeriesPlot metric="cpuLoad" />
                <Incidents metric="cpuLoad" />
              </AlertController>
            </PollingController>
          </Dashboard>

          <AlertPermissionPrompt />
        </MainContent>
      </Container>
    );
  }
}

interface Props {
  getCpuMetrics: typeof actions.collectors.getCpuMetrics;
  sampleRate: number;
}

const Container = styled.div`
  display: flex;
  flex-grow: 1;
`;

const MainContent = styled.main.attrs({ role: 'main' })`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
`;

const Dashboard = styled.div`
  flex-grow: 1;
  padding: 2rem;
  display: grid;
  grid-template-columns: minmax(0, 1fr) 1fr;
  grid-template-rows: auto;
  align-items: start;
  gap: 2rem;
`;

export function mapStateToProps(state: State) {
  return {
    sampleRate: state.metrics.cpuLoad.sampleRate,
  };
}

const mapDispatchToProps = {
  getCpuMetrics: actions.collectors.getCpuMetrics,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
