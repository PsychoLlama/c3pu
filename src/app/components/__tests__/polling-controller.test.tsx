import React from 'react';
import { shallow } from 'enzyme';
import PollingController from '../polling-controller';

jest.useFakeTimers('modern');

describe('PollingController', () => {
  function setup(
    overrides?: Partial<React.ComponentProps<typeof PollingController>>,
  ) {
    const props = {
      query: jest.fn(),
      sampleRate: 10,
      ...overrides,
    };

    const output = shallow(<PollingController {...props} />);

    return {
      output,
      props,
    };
  }

  it('renders whatever children it is given', () => {
    const Child = () => null;
    const { output } = setup({ children: <Child /> });

    expect(output.find(Child).exists()).toBe(true);
  });

  it('immediately dispatches a request on mount', () => {
    const { props } = setup();

    expect(props.query).toHaveBeenCalled();
  });

  it('schedules the next sampling', () => {
    const { props } = setup();

    jest.advanceTimersByTime(props.sampleRate * 1000 - 1);
    expect(props.query).toHaveBeenCalledTimes(1);
    jest.advanceTimersByTime(1);
    expect(props.query).toHaveBeenCalledTimes(2);
    jest.advanceTimersByTime(props.sampleRate * 1000);
    expect(props.query).toHaveBeenCalledTimes(3);
  });

  it('only samples data while mounted', () => {
    const { output, props } = setup();

    expect(props.query).toHaveBeenCalledTimes(1);
    output.unmount();

    jest.advanceTimersByTime(props.sampleRate * 1000);
    expect(props.query).toHaveBeenCalledTimes(1);
  });

  it('dynamically changes the sample rate when requested', () => {
    const { output, props } = setup({ sampleRate: 10 });

    output.setProps({ sampleRate: 5 });

    jest.advanceTimersByTime(5_000);
    expect(props.query).toHaveBeenCalledTimes(2);
  });
});
