import React from 'react';
import { shallow } from 'enzyme';
import produce from 'immer';
import { Incidents, mapStateToProps } from '../incidents';
import initialState from '../../reducers/initial-state';

// I wish Jest would set this by default.
if (process.env.TZ !== 'UTC') {
  throw new Error(
    `Tests must run in UTC time. Set TZ=UTC before running the suite.`,
  );
}

describe('Incidents', () => {
  function setup(overrides?: Partial<React.ComponentProps<typeof Incidents>>) {
    const props = {
      metric: 'cpuLoad' as const,
      incidents: [
        { firstNoticed: 1, lastReported: 5, resolved: true },
        { firstNoticed: 15, lastReported: 35, resolved: false },
      ],
      ...overrides,
    };

    const output = shallow(<Incidents {...props} />);

    return {
      output,
      props,
    };
  }

  it('shows no incidents with healthy operating data', () => {
    const { output } = setup({ incidents: [] });

    expect(output.find({ 'data-test': 'no-incidents' }).exists()).toBe(true);
  });

  it('lists every incident', () => {
    const { output, props } = setup({
      incidents: [
        { firstNoticed: 1, lastReported: 5, resolved: true },
        { firstNoticed: 15, lastReported: 35, resolved: false },
      ],
    });

    const incidents = output.find({ 'data-test': 'incident' });

    expect(output.find({ 'data-test': 'no-incidents' }).exists()).toBe(false);
    expect(incidents.length).toBe(props.incidents.length);
  });

  it('shows information about the incident', () => {
    const { output } = setup({
      incidents: [
        {
          firstNoticed: 1434369600,
          lastReported: 1434369600 + 120,
          resolved: true,
        },
      ],
    });

    const time = output.find({ 'data-test': 'incident-time' });
    const lastReport = output.find({ 'data-test': 'incident-last-report' });
    const resolved = output.find({ 'data-test': 'incident-resolved' });

    expect(time.text()).toBe('12:00 PM');
    expect(lastReport.text()).toBe('12:02 PM');
    expect(resolved.text()).toBe('Yes');
  });

  it('shows newest incidents on top', () => {
    const { output } = setup({
      incidents: [
        {
          // Oldest
          firstNoticed: 1434369600 - 100,
          lastReported: 1434369600 - 80,
          resolved: true,
        },
        {
          // Newest
          firstNoticed: 1434369600,
          lastReported: 1434369600 + 20,
          resolved: true,
        },
      ],
    });

    const time = output.find({ 'data-test': 'incident-time' }).first();

    expect(time.text()).toBe('12:00 PM');
  });

  describe('mapStateToProps', () => {
    it('returns the expected state', () => {
      const state = produce(initialState, (draft) => {
        draft.metrics.cpuLoad.dataPoints.push({ timestamp: 1, value: 10 });
      });

      const props = mapStateToProps(state, { metric: 'cpuLoad' });

      expect(props).toMatchInlineSnapshot(`
        Object {
          "incidents": Array [],
        }
      `);
    });
  });
});
