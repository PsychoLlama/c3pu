import React from 'react';
import { shallow } from 'enzyme';
import {
  AlertPermissionPrompt,
  mapStateToProps,
} from '../alert-permission-prompt';
import initialState from '../../reducers/initial-state';

describe('AlertPermissionPrompt', () => {
  function setup(
    overrides?: Partial<React.ComponentProps<typeof AlertPermissionPrompt>>,
  ) {
    const props = {
      requestPermission: jest.fn(),
      permission: 'default' as const,
      ...overrides,
    };

    const output = shallow(<AlertPermissionPrompt {...props} />);

    return {
      output,
      props,
    };
  }

  it('requests permission when you click the enable button', () => {
    const { output, props } = setup();

    expect(props.requestPermission).not.toHaveBeenCalled();
    output.find({ 'data-test': 'enable-button' }).simulate('click');
    expect(props.requestPermission).toHaveBeenCalled();
  });

  it('disables the button while requesting permission', () => {
    const { output } = setup({ permission: 'requesting' });

    const { disabled } = output.find({ 'data-test': 'enable-button' }).props();

    expect(disabled).toBe(true);
  });

  it('shows a warning when permissions are disabled', () => {
    const { output } = setup({ permission: 'denied' });

    const button = output.find({ 'data-test': 'enable-button' });
    const message = output.find({ 'data-test': 'message' });

    expect(button.exists()).toBe(false);
    expect(message.text()).toMatch(/blocked by your browser/i);
  });

  it('hides the prompt when permission is given', () => {
    const { output } = setup({ permission: 'granted' });

    expect(output.isEmptyRender()).toBe(true);
  });

  describe('mapStateToProps', () => {
    it('grabs the expected props', () => {
      const props = mapStateToProps(initialState);

      expect(props).toMatchInlineSnapshot(`
        Object {
          "permission": "default",
        }
      `);
    });
  });
});
