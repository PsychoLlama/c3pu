import React from 'react';
import { shallow } from 'enzyme';
import { SizeObserver } from '../size-observer';

describe('SizeObserver', () => {
  function setup(
    overrides?: Partial<React.ComponentProps<typeof SizeObserver>>,
  ) {
    const props = {
      children: <div />,
      onResize: jest.fn(),
      ...overrides,
    };

    const output = shallow(<SizeObserver {...props} />);

    return {
      output,
      props,

      /** Invoke the `ref` handler for the wrapper div */
      simulateRef(ref: any = { mock: 'dom-element' }) {
        (output.getElement() as any).ref?.(ref);

        return ref;
      },

      /** Get the component's resize observer */
      getObserver() {
        expect(ResizeObserver).toHaveBeenCalled();
        return (ResizeObserver as any).mock.results[0].value;
      },

      /** Get the callback passed to component's resize observer */
      getCallback() {
        expect(ResizeObserver).toHaveBeenCalled();
        return (ResizeObserver as any).mock.calls[0][0];
      },
    };
  }

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('renders the children', () => {
    const Child = () => null;
    const { output } = setup({ children: <Child /> });

    expect(output.find(Child).exists()).toBe(true);
  });

  it('watches the child for dimension changes', () => {
    const { props, simulateRef, getCallback } = setup();

    simulateRef();
    const dimensions = { width: 20, height: 10 };
    getCallback()([{ contentRect: dimensions }]);

    expect(props.onResize).toHaveBeenCalledWith(dimensions);
  });

  it('syncs the resize observer to the child element', () => {
    const { simulateRef, getObserver } = setup();
    const observer = getObserver();

    expect(observer.observe).not.toHaveBeenCalled();
    const ref1 = simulateRef({ ref: 1 });
    expect(observer.observe).toHaveBeenCalledWith(ref1);
    simulateRef(null);
    expect(observer.unobserve).toHaveBeenCalledWith(ref1);
    const ref2 = simulateRef({ ref: 2 });
    expect(observer.observe).toHaveBeenCalledWith(ref2);
  });
});
