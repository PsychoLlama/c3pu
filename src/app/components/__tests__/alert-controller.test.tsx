import React from 'react';
import { shallow } from 'enzyme';
import produce from 'immer';
import { AlertController, mapStateToProps } from '../alert-controller';
import initialState from '../../reducers/initial-state';
import { Incident } from '../../selectors/incidents';

describe('AlertController', () => {
  function setup(
    overrides?: Partial<React.ComponentProps<typeof AlertController>>,
  ) {
    const props = {
      incidents: [],
      metric: 'cpuLoad' as const,
      label: 'CPU',
      timeToEscalation: 10,
      timeToCooldown: 20,
      ...overrides,
    };

    const output = shallow(<AlertController {...props} />);

    return {
      output,
      props,
    };
  }

  function createIncident(overrides?: Partial<Incident>) {
    return {
      firstNoticed: 1,
      lastReported: 1 + 120,
      resolved: false,
      ...overrides,
    };
  }

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('renders arbitrary children', () => {
    const Child = () => null;
    const { output } = setup({ children: <Child /> });

    expect(output.find(Child).exists()).toBe(true);
  });

  it('sends a notification when a new incident is reported', () => {
    const { output } = setup();

    expect(Notification).not.toHaveBeenCalled();

    const incident = createIncident();
    output.setProps({ incidents: [incident] });
    expect(Notification).toHaveBeenCalledWith(
      expect.stringMatching(/levels unhealthy/i),
      expect.anything(),
    );
  });

  it('sends a notification when incidents are resolved', () => {
    const { output } = setup({ incidents: [createIncident()] });

    expect(Notification).not.toHaveBeenCalled();
    output.setProps({ incidents: [createIncident({ resolved: true })] });
    expect(Notification).toHaveBeenCalledWith(
      expect.stringMatching(/levels recovered/i),
      expect.anything(),
    );
  });

  it('does not emit false positives as old data points are deleted', () => {
    const { output } = setup({
      incidents: [createIncident({ firstNoticed: 1 })],
    });

    output.setProps({ incidents: [createIncident({ firstNoticed: 2 })] });

    expect(Notification).not.toHaveBeenCalled();
  });

  describe('mapStateToProps', () => {
    it('returns the expected state', () => {
      const state = produce(initialState, (state) => {
        state.metrics.cpuLoad.monitoring.timeToEscalation = 1;
        state.metrics.cpuLoad.dataPoints.push(
          { timestamp: 1, value: 500 },
          { timestamp: 100, value: 500 },
        );
      });

      const props = mapStateToProps(state, { metric: 'cpuLoad', label: 'CPU' });

      expect(props).toMatchInlineSnapshot(`
        Object {
          "incidents": Array [
            Object {
              "firstNoticed": 1,
              "lastReported": 100,
              "resolved": false,
            },
          ],
          "timeToCooldown": 120,
          "timeToEscalation": 1,
        }
      `);
    });
  });
});
