import React from 'react';
import { shallow } from 'enzyme';
import { App, mapStateToProps } from '../app';
import initialState from '../../reducers/initial-state';

describe('App', () => {
  function setup(overrides?: Partial<React.ComponentProps<typeof App>>) {
    const props = {
      getCpuMetrics: jest.fn(),
      sampleRate: 10,
      ...overrides,
    };

    const output = shallow(<App {...props} />);

    return {
      output,
      props,
    };
  }

  it('renders', () => {
    expect(setup).not.toThrow();
  });

  describe('mapStateToProps', () => {
    it('returns the expected state', () => {
      const props = mapStateToProps(initialState);

      expect(props).toMatchInlineSnapshot(`
        Object {
          "sampleRate": 10,
        }
      `);
    });
  });
});
