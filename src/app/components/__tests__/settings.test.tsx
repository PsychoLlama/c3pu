import React from 'react';
import { shallow } from 'enzyme';
import { Settings, mapStateToProps } from '../settings';
import initialState from '../../reducers/initial-state';

describe('Settings', () => {
  function setup(overrides?: Partial<React.ComponentProps<typeof Settings>>) {
    const props = {
      updateSampleRate: jest.fn(),
      updateSamplePeriod: jest.fn(),
      updateUpperThreshold: jest.fn(),
      updateEscalationDelay: jest.fn(),
      updateCooldownDelay: jest.fn(),
      samplePeriod: 60,
      sampleRate: 10,
      upperThreshold: 100,
      timeToEscalation: 120,
      timeToCooldown: 180,
      ...overrides,
    };

    const output = shallow(<Settings {...props} />);

    return {
      output,
      props,
    };
  }

  beforeEach(() => {
    process.env.NODE_ENV = 'development';
  });

  it('updates the sample rate/period when the inputs change', () => {
    const { output, props } = setup();

    output.find({ 'data-test': 'sample-rate-input' }).simulate('change', 6);
    expect(props.updateSampleRate).toHaveBeenCalledWith({
      metric: 'cpuLoad',
      value: 6,
    });

    output.find({ 'data-test': 'sample-period-input' }).simulate('change', 60);
    expect(props.updateSamplePeriod).toHaveBeenCalledWith({
      metric: 'cpuLoad',
      value: 60,
    });
  });

  it('hides the panel in production', () => {
    process.env.NODE_ENV = 'production';
    const { output } = setup();

    expect(output.isEmptyRender()).toBe(true);
  });

  describe('mapStateToProps', () => {
    it('grabs the necessary props', () => {
      const props = mapStateToProps(initialState);

      expect(props).toMatchInlineSnapshot(`
        Object {
          "samplePeriod": 600,
          "sampleRate": 10,
          "timeToCooldown": 120,
          "timeToEscalation": 120,
          "upperThreshold": 100,
        }
      `);
    });
  });
});
