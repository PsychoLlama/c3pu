import React from 'react';
import { connect } from 'react-redux';
import { State } from '../reducers/initial-state';
import { getIncidents, Incident } from '../selectors/incidents';

/**
 * This component provides alerting. If an incident starts or ends, it'll
 * dispatch a notification.
 *
 * Writing this as a component (rather than a utility) makes it easier to bind
 * alerting to an arbitrary lifecycle. There may be conditions where you only
 * want alerts to show if you're actively looking at the data being monitored.
 */
export class AlertController extends React.Component<Props> {
  componentDidUpdate(prevProps: Props) {
    const { incidents, label, timeToEscalation, timeToCooldown } = this.props;
    const lastIncident = prevProps.incidents[prevProps.incidents.length - 1];
    const currentIncident = incidents[incidents.length - 1];

    if (
      currentIncident?.resolved === false &&
      lastIncident?.resolved !== false
    ) {
      new Notification(`${label} Levels Unhealthy`, {
        body: `Levels have been above the threshold for over ${timeToEscalation} seconds.`,
      });
    }

    if (lastIncident?.resolved === false && currentIncident?.resolved) {
      new Notification(`${label} Levels Recovered`, {
        body: `Levels have been below the threshold for over ${timeToCooldown} seconds.`,
      });
    }
  }

  render() {
    return this.props.children;
  }
}

interface OwnProps {
  metric: keyof State['metrics'];
  children?: React.ReactNode;

  /** Human-readable description of the metric being measured. */
  label: string;
}

interface Props extends OwnProps {
  incidents: Array<Incident>;
  timeToEscalation: number;
  timeToCooldown: number;
}

export function mapStateToProps(state: State, { metric }: OwnProps) {
  const { timeToEscalation, timeToCooldown } = state.metrics[metric].monitoring;

  return {
    incidents: getIncidents({ state, metric }),
    timeToEscalation,
    timeToCooldown,
  };
}

export default connect(mapStateToProps)(AlertController);
