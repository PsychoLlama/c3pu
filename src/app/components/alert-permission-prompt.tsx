import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import * as css from '../utils/css';
import { Button } from './core';
import * as actions from '../actions';
import { State, AlertsPermission } from '../reducers/initial-state';

export class AlertPermissionPrompt extends React.Component<Props> {
  render() {
    const { permission } = this.props;

    if (permission === 'granted') {
      return null;
    }

    return (
      <Bubble data-permission={permission}>
        {permission === 'denied' ? (
          <Message data-test="message">
            Alerts are blocked by your browser. You will not receive
            notifications.
          </Message>
        ) : (
          <>
            <Message data-test="message">
              This app needs permission before it can send you alerts.
            </Message>
            <RequestAccess
              data-test="enable-button"
              onClick={this.getPermission}
              disabled={permission === 'requesting'}
            >
              Enable
            </RequestAccess>
          </>
        )}
      </Bubble>
    );
  }

  // Wrapped to avoid sending an argument.
  private getPermission = () => this.props.requestPermission();
}

interface Props {
  requestPermission: typeof actions.alerts.requestPermission;
  permission: AlertsPermission;
}

const Bubble = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${css.color('tertiary')};
  background-color: ${css.color('tertiary-overlay')};
  border-radius: 3px;
  padding: 1rem;
  margin: 1rem;
  gap: 1rem;
  max-width: 24rem;
  align-self: end;

  &[data-permission='denied'] {
    border: 1px solid ${css.color('secondary')};
    background-color: ${css.color('secondary-overlay')};
  }
`;

const Message = styled.p`
  margin: 0;
  line-height: 1.4rem;
`;

const RequestAccess = styled(Button)`
  background-color: ${css.color('tertiary')};
  color: ${css.color('black')};
  box-shadow: 0 2px 4px ${css.color('dark')};
  padding: 0.5rem 1rem;
  border-radius: 3px;
  align-self: end;
`;

const mapDispatchToProps = {
  requestPermission: actions.alerts.requestPermission,
};

export function mapStateToProps(state: State) {
  return {
    permission: state.alerts.permission,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlertPermissionPrompt);
