import { OneDark } from '../../utils/themes';

export function renderLegendLabel(_plot: uPlot, value: null | number) {
  return value ? `${value.toFixed(2)}%` : 'No data';
}

export function renderAxisRange(_plot: uPlot, values: Array<number>) {
  return values.map((value) => `${value}%`);
}

/**
 * uPlot will try to start the chart at the lowest and highest values known,
 * which can create some pretty confusing charts. At first glance an 80% value
 * might look small if it's at the bottom of the chart.
 *
 * This function fixes the range to 0-100, but allows growth if it goes
 * higher.
 */
export const getYAxisRange: uPlot.Range.Function = (
  _plot: uPlot,
  _min: number,
  max: number,
) => [0, Math.max(100, max)];

export default {
  scales: {
    '%': {
      auto: true,
      range: getYAxisRange,
    },
  },
  cursor: {
    // Zooming is a confusing feature if you're not expecting it.
    drag: { x: false, y: false },
  },
  series: [
    {},
    {
      label: 'CPU Load',
      scale: '%',
      width: 2 / devicePixelRatio,
      stroke: OneDark.primary,
      value: renderLegendLabel,
    },
    {
      label: 'Threshold',
      scale: '%',
      width: 1 / devicePixelRatio,
      stroke: OneDark.quaternary,
      value: renderLegendLabel,
      points: { show: false },
    },
  ],
  bands: [
    {
      // Highlights recorded values above the threshold in red.
      series: [1, 2] as uPlot.Band.Bounds,
      fill: OneDark['secondary-overlay'],
    },
  ],
  axes: [
    {
      stroke: OneDark.text,
    },
    {
      scale: '%',
      side: 1,
      size: 60,
      stroke: OneDark.text,
      values: renderAxisRange,
    },
    {},
  ],
};
