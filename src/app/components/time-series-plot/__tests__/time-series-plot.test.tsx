import React from 'react';
import { shallow } from 'enzyme';
import Chart from 'uplot-react';
import { TimeSeriesPlot, mapStateToProps } from '../time-series-plot';
import initialState from '../../../reducers/initial-state';

jest.useFakeTimers('modern');
jest.setSystemTime(100_000);

describe('TimeSeriesPlot', () => {
  function setup(
    overrides?: Partial<React.ComponentProps<typeof TimeSeriesPlot>>,
  ) {
    const props = {
      metric: 'cpuLoad' as const,
      dataPoints: [],
      sampleRate: 10,
      samplePeriod: 60,
      upperThreshold: 15,
      ...overrides,
    };

    const output = shallow(<TimeSeriesPlot {...props} />);

    return {
      output,
      props,
    };
  }

  it('fills in chart data', () => {
    const { output, props } = setup({
      sampleRate: 10,
      samplePeriod: 50,
      dataPoints: [
        { timestamp: 40, value: 1 },
        { timestamp: 50, value: 2 },
      ],
    });

    const { data } = output.find(Chart).props();

    expect(data).toHaveLength(3);
    expect(data[0]).toEqual([10, 20, 30, 40, 50]);
    expect(data[1]).toEqual([null, null, null, 1, 2]);
    expect(data[2]).toEqual(Array(5).fill(props.upperThreshold));
  });

  it('uses system time as the most recent record for empty datasets', () => {
    const { output } = setup({
      sampleRate: 10,
      samplePeriod: 40,
      dataPoints: [],
    });

    const { data } = output.find(Chart).props();

    // Current time is mocked at 100 seconds epoch.
    expect(data[0]).toEqual([60, 70, 80, 90]);
  });

  it('rounds up when the sample rate does not divide evenly', () => {
    const { output } = setup({
      sampleRate: 7,
      samplePeriod: 22,
    });

    const { data } = output.find(Chart).props();

    expect(data[0]).toHaveLength(4);
  });

  it('does not throw a fit when there are an unexpected number of rows', () => {
    const render = () =>
      setup({
        sampleRate: 5,
        samplePeriod: 10,

        // Given the rate/period, we'd only expect 2 items.
        dataPoints: [
          { timestamp: 10, value: 1 },
          { timestamp: 20, value: 2 },
          { timestamp: 30, value: 3 },
        ],
      });

    expect(render).not.toThrow();
  });

  it('binds the parent container width to the canvas', () => {
    const { output } = setup();

    output.find({ 'data-test': 'size-observer' }).simulate('resize', {
      width: 995,
    });

    const { width } = output.find(Chart).prop('options');

    expect(width).toBe(995);
  });

  describe('mapStateToProps', () => {
    it('grabs the correct state', () => {
      const props = mapStateToProps(initialState, { metric: 'cpuLoad' });

      expect(props).toMatchInlineSnapshot(`
        Object {
          "dataPoints": Array [],
          "samplePeriod": 600,
          "sampleRate": 10,
          "upperThreshold": 100,
        }
      `);
    });
  });
});
