import {
  renderAxisRange,
  renderLegendLabel,
  getYAxisRange,
} from '../uplot-options';

describe('uPlot Options', () => {
  describe('renderAxisRange', () => {
    it('returns a list of labels', () => {
      const labels = renderAxisRange(null as any, [60, 80, 100]);

      expect(labels).toEqual(['60%', '80%', '100%']);
    });
  });

  describe('renderLegendLabel', () => {
    it('conditionally shows the active value', () => {
      expect(renderLegendLabel(null as any, 10)).toBe('10.00%');
      expect(renderLegendLabel(null as any, 1.23456789)).toBe('1.23%');
      expect(renderLegendLabel(null as any, null)).toBe('No data');
    });
  });

  describe('chooseRangeSize', () => {
    it('clamps the values to 0-100 for healthy operating data', () => {
      const range = getYAxisRange(null as any, 20, 80, '%');

      expect(range).toEqual([0, 100]);
    });

    it('uses the detected maximum when it exceeds the default', () => {
      const range = getYAxisRange(null as any, 20, 150, '%');

      expect(range).toEqual([0, 150]);
    });
  });
});
