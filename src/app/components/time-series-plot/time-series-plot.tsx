import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import uPlot from 'uplot';
import Chart from 'uplot-react';
import 'uplot/dist/uPlot.min.css';
import { State as ReduxState, DataPoint } from '../../reducers/initial-state';
import * as css from '../../utils/css';
import uPlotOptions from './uplot-options';
import SizeObserver, { BoundingBox } from '../size-observer';

export class TimeSeriesPlot extends React.Component<Props, State> {
  mountTimestamp = new Date().getTime() / 1000; // Unix epoch.
  state = {
    width: 0,
    height: 400,
  };

  render() {
    return (
      <Container>
        <SizeObserver data-test="size-observer" onResize={this.updateWidth}>
          <Chart options={this.getChartOptions()} data={this.getChartData()} />
        </SizeObserver>
      </Container>
    );
  }

  private getChartOptions = () => ({
    ...uPlotOptions,
    width: this.state.width,
    height: this.state.height,
  });

  private updateWidth = ({ width }: BoundingBox) => {
    this.setState({ width });
  };

  // Note: generating a new array causes uplot-react to audit the entire data
  // source to detect changes. If performance becomes a problem, consider
  // memoizing against `dataPoints`.
  private getChartData = (): uPlot.AlignedData => [
    this.getTimeAxis(),
    this.getYAxis(),
    this.genMissingData([]).fill(this.props.upperThreshold),
  ];

  private getTimeAxis = (): Timeline => {
    const timestamps = this.props.dataPoints.map((point) => point.timestamp);
    const firstRecordTimestamp = timestamps[0] || this.mountTimestamp;

    // If we don't have enough records to fill the observational time window,
    // fill the gap with records assuming a constant sample rate going back in
    // time from the first known record (or time from mount, if we don't have
    // any data).
    return this.genMissingData(timestamps)
      .reduceRight((syntheticRecords: Array<number>) => {
        const [nextRecordTimestamp = firstRecordTimestamp] = syntheticRecords;
        return [nextRecordTimestamp - this.props.sampleRate].concat(
          syntheticRecords,
        );
      }, [])
      .concat(timestamps);
  };

  private getYAxis = (): ChartYValues => {
    const vector = this.props.dataPoints.map((point) => point.value);
    return this.genMissingData(vector).concat(vector);
  };

  /**
   * If our window allows for 30 data points but we only have 5, this function
   * adds 25 blank records for consistent chart size.
   */
  private genMissingData = (column: ChartYValues): ChartYValues => {
    const { sampleRate, samplePeriod } = this.props;
    const expectedRecordCount = Math.ceil(samplePeriod / sampleRate);

    // If the sample rate/period was just altered, we may have more records
    // than we expect. Clamping the value avoids making an array of -1 items.
    const missingRecordCount = Math.max(0, expectedRecordCount - column.length);

    return Array(missingRecordCount).fill(null);
  };
}

// Think of this as a spreadsheet. The outer array is a list of columns, the
// inner arrays are lists of rows. The first column is always a unix epoch.
// This is the structure uPlot expects.
type Timeline = Array<number>; // Unix epoch timestamps.
type ChartYValues = Array<null | number>; // Values over time.

interface OwnProps {
  metric: keyof ReduxState['metrics'];
}

interface Props extends OwnProps {
  dataPoints: Array<DataPoint>;
  sampleRate: number;
  samplePeriod: number;
  upperThreshold: number;
}

interface State {
  width: number;
  height: number;
}

const Container = styled.div`
  background-color: ${css.color('dark')};
  padding: 1rem;
  border-radius: 3px;
  box-shadow: 0 2px 4px ${css.color('dark')};
  border: 1px solid ${css.color('darkest')};
`;

export function mapStateToProps(state: ReduxState, props: OwnProps) {
  const metric = state.metrics[props.metric];

  return {
    dataPoints: metric.dataPoints,
    sampleRate: metric.sampleRate,
    samplePeriod: metric.samplePeriod,
    upperThreshold: metric.monitoring.threshold.upperBound,
  };
}

export default connect(mapStateToProps)(TimeSeriesPlot);
