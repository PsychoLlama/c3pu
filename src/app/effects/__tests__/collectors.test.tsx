import * as collectors from '../collectors';
import fetchMock from 'jest-fetch-mock';

jest.useFakeTimers('modern');
jest.setSystemTime(1434369600000);

describe('Collector effects', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('getCpuMetrics', () => {
    beforeEach(() => {
      const payload = JSON.stringify({ loadAverage: 0.75 });
      fetchMock.mockResponse(payload);
    });

    it('grabs the CPU metrics', async () => {
      const response = await collectors.getCpuMetrics();

      expect(fetch).toHaveBeenCalledWith('/api/v1/metrics/cpu');
      expect(response).toEqual({
        timestamp: 1434369600,
        data: { loadAverage: 0.75 },
      });
    });
  });
});
