export async function getCpuMetrics() {
  const response = await fetch('/api/v1/metrics/cpu');

  return {
    timestamp: Math.round(new Date().getTime() / 1000),
    data: await response.json(),
  };
}
