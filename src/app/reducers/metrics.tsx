import { createReducer } from 'retreon';
import initialState, { Metric } from './initial-state';
import * as actions from '../actions';

// Note: Retreon wraps reducers with Immer. This isn't mutation, it is magic.
export default createReducer(initialState.metrics, (handleAction) => [
  handleAction(actions.collectors.getCpuMetrics, (state, payload) => {
    state.cpuLoad.dataPoints.push({
      timestamp: payload.timestamp,

      // Load average is a number between 0 and 1 (and sometimes more)
      // representing the usage percentage, but it looks more conventional on
      // a chart at a scale of 0-100.
      value: payload.data.loadAverage * 100,
    });

    // Discard old data points when we have greater than `N` records.
    state.cpuLoad.dataPoints = state.cpuLoad.dataPoints.slice(
      -getMaxRecords(state.cpuLoad),
    );
  }),

  handleAction(
    actions.settings.updateSampleRate,
    (state, { metric, value }) => {
      state[metric].sampleRate = value;
    },
  ),

  handleAction(
    actions.settings.updateSamplePeriod,
    (state, { metric, value }) => {
      state[metric].samplePeriod = value;
    },
  ),

  handleAction(
    actions.settings.updateUpperThreshold,
    (state, { metric, value }) => {
      state[metric].monitoring.threshold.upperBound = value;
    },
  ),

  handleAction(
    actions.settings.updateEscalationDelay,
    (state, { metric, value }) => {
      state[metric].monitoring.timeToEscalation = value;
    },
  ),

  handleAction(
    actions.settings.updateCooldownDelay,
    (state, { metric, value }) => {
      state[metric].monitoring.timeToCooldown = value;
    },
  ),
]);

/**
 * Determines the maximum number of records in a dataset, considering the
 * sample rate over the predetermined period.
 */
function getMaxRecords(metric: Metric) {
  return Math.ceil(metric.samplePeriod / metric.sampleRate);
}
