import { initialize as createStore } from '../../utils/redux-store';
import * as actions from '../../actions';

// Technically already a mock, but TypeScript didn't know that.
const requestPermission = jest.spyOn(Notification, 'requestPermission');

describe('Alerts reducer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    requestPermission.mockResolvedValue('granted');
  });

  describe('alerts.requestPermission', () => {
    it('sets the new permission', async () => {
      const store = createStore();

      await store.dispatch(actions.alerts.requestPermission());

      expect(store.getState()).toHaveProperty('alerts', {
        permission: 'granted',
      });
    });
  });
});
