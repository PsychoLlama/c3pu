import produce from 'immer';
import { initialize as createStore } from '../../utils/redux-store';
import * as actions from '../../actions';
import * as mockedCollectors from '../../effects/collectors';
import initialState from '../initial-state';

jest.mock('../../effects/collectors');

const collectors: jest.Mocked<typeof mockedCollectors> =
  mockedCollectors as any;

describe('Metrics reducer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    collectors.getCpuMetrics.mockResolvedValue({
      timestamp: 123,
      data: { loadAverage: 0.75 },
    });
  });

  describe('collectors.getCpuMetrics', () => {
    it('stores the new data point', async () => {
      const store = createStore();

      await store.dispatch(actions.collectors.getCpuMetrics());

      expect(store.getState()).toMatchObject({
        metrics: {
          cpuLoad: {
            dataPoints: [{ value: 75, timestamp: 123 }],
          },
        },
      });
    });

    it('purges old items outside the monitoring window', async () => {
      const store = createStore(
        produce(initialState, (state) => {
          state.metrics.cpuLoad.sampleRate = 10;
          state.metrics.cpuLoad.samplePeriod = 60;
          state.metrics.cpuLoad.dataPoints = Array(6)
            .fill(null)
            .map((_, i) => ({
              timestamp: i,
              value: i,
            }));
        }),
      );

      await store.dispatch(actions.collectors.getCpuMetrics());

      expect(store.getState().metrics.cpuLoad.dataPoints).toHaveLength(6);
      expect(store.getState().metrics.cpuLoad.dataPoints[0]).not.toMatchObject({
        value: 0,
      });
    });
  });

  describe('settings.updateSampleRate', () => {
    it(`updates the metric's sample rate`, async () => {
      const store = createStore();

      store.dispatch(
        actions.settings.updateSampleRate({
          metric: 'cpuLoad',
          value: 6,
        }),
      );

      expect(store.getState().metrics.cpuLoad).toMatchObject({
        sampleRate: 6,
      });
    });
  });

  describe('settings.updateSamplePeriod', () => {
    it(`updates the metric's sample period`, () => {
      const store = createStore();

      store.dispatch(
        actions.settings.updateSamplePeriod({
          metric: 'cpuLoad',
          value: 60,
        }),
      );

      expect(store.getState().metrics.cpuLoad).toMatchObject({
        samplePeriod: 60,
      });
    });
  });

  describe('settings.updateUpperThreshold', () => {
    it('updates the monitoring threshold', () => {
      const store = createStore();

      store.dispatch(
        actions.settings.updateUpperThreshold({
          metric: 'cpuLoad',
          value: 150,
        }),
      );

      const { monitoring } = store.getState().metrics.cpuLoad;
      expect(monitoring.threshold).toMatchObject({
        upperBound: 150,
      });
    });
  });

  describe('settings.update{Escalation,Cooldown}Delay', () => {
    it('updates the monitoring threshold', () => {
      const store = createStore();

      store.dispatch(
        actions.settings.updateEscalationDelay({
          metric: 'cpuLoad',
          value: 20,
        }),
      );

      store.dispatch(
        actions.settings.updateCooldownDelay({
          metric: 'cpuLoad',
          value: 10,
        }),
      );

      const { monitoring } = store.getState().metrics.cpuLoad;
      expect(monitoring).toMatchObject({
        timeToEscalation: 20,
        timeToCooldown: 10,
      });
    });
  });
});
