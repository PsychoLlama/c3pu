import { createReducer } from 'retreon';
import initialState from './initial-state';
import * as actions from '../actions';

export default createReducer(initialState.alerts, (handleAction) => [
  handleAction.optimistic(actions.alerts.requestPermission, (state) => {
    state.permission = 'requesting';
  }),

  handleAction(actions.alerts.requestPermission, (state, permission) => {
    state.permission = permission;
  }),
]);
