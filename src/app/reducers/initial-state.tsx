export interface State {
  metrics: {
    cpuLoad: Metric;
  };

  alerts: {
    permission: AlertsPermission;
  };
}

export interface Metric {
  /** How frequently to measure the metric, measured in seconds. */
  sampleRate: number;

  /** The period of time over which the metric is viewed, measured in seconds. */
  samplePeriod: number;

  dataPoints: Array<DataPoint>;

  monitoring: {
    /** Number of seconds beyond a threshold before we sound the alarm. */
    timeToEscalation: number;

    /** Number of seconds in a healthy zone before the issue is resolved. */
    timeToCooldown: number;

    threshold: {
      /** An upper limit where a measure becomes concerning. */
      upperBound: number;
    };
  };
}

export interface DataPoint {
  /** Unix epoch value, in seconds. */
  timestamp: number;

  /** An arbitrary numberical value representing the data point. */
  value: number;
}

export type AlertsPermission = NotificationPermission | 'requesting';

const initialState: State = {
  metrics: {
    cpuLoad: {
      dataPoints: [],
      sampleRate: 10, // every 10 seconds
      samplePeriod: 60 * 10, // 10 minute window
      monitoring: {
        timeToEscalation: 60 * 2, // 2 minutes
        timeToCooldown: 60 * 2, // 2 minutes
        threshold: {
          upperBound: 100,
        },
      },
    },
  },

  alerts: {
    permission: Notification.permission,
  },
};

export default initialState;
