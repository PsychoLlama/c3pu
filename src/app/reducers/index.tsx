import { combineReducers } from 'redux';
import alertsReducer from './alerts';
import metricsReducer from './metrics';

export default combineReducers({
  alerts: alertsReducer,
  metrics: metricsReducer,
});
