export interface Theme {
  background: string;
  foreground: string;
  text: string;
  black: string;
  primary: string;
  secondary: string;
  tertiary: string;
  quaternary: string;
  white: string;
  dark: string;
  darkest: string;
  'secondary-overlay': string;
  'tertiary-overlay': string;
}

export const OneDark: Theme = {
  background: '#282C34',
  foreground: '#ABB2BF',
  text: '#ABB2BF',
  black: '#282C34',
  primary: '#61AFEF',
  secondary: '#E06C75',
  tertiary: '#98C379',
  quaternary: '#E5C07B',
  white: '#ABB2BF',
  dark: 'rgba(0, 0, 0, 0.2)',
  darkest: 'rgba(0, 0, 0, 0.4)',
  'secondary-overlay': '#E06C7518',
  'tertiary-overlay': '#98C37911',
};

// Variable names must stay in sync with `css.color(...)` utility.
export function exportToCss(theme: Theme) {
  return `
    --color-background: ${theme.background};
    --color-foreground: ${theme.foreground};
    --color-text: ${theme.text};

    --color-black: ${theme.black};
    --color-white: ${theme.white};

    --color-dark: ${theme.dark};
    --color-darkest: ${theme.darkest};

    --color-primary: ${theme.primary};
    --color-secondary: ${theme.secondary};
    --color-tertiary: ${theme.tertiary};
    --color-quaternary: ${theme.quaternary};

    --color-secondary-overlay: ${theme['secondary-overlay']};
    --color-tertiary-overlay: ${theme['tertiary-overlay']};
  `;
}
