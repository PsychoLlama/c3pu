import { Theme } from './themes';

/** This provides type-safe access to CSS variables. */
export function color(id: keyof Theme): string {
  return `var(--color-${id})`;
}
