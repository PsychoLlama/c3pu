import { exportToCss, OneDark } from '../themes';

// Just basic sanity checks. If this fails, it will be *very* obvious.
describe('exportToCss', () => {
  it('can serialize all themes', () => {
    expect(() => exportToCss(OneDark)).not.toThrow();
  });
});
