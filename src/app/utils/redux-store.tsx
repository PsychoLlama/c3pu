import { createStore, compose, applyMiddleware } from 'redux';
import { middleware as retreonMiddleware } from 'retreon';
import reducer from '../reducers';
import { State } from '../reducers/initial-state';

// Enables integration with the redux-devtools browser extension.
const DEVTOOLS_KEY = '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__';

export function initialize(state?: State) {
  const composeEnhancers = window[DEVTOOLS_KEY] || compose;
  const enhancer = composeEnhancers(applyMiddleware(retreonMiddleware));

  return createStore(reducer, state, enhancer);
}

export default initialize();
