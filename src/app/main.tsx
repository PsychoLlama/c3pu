import DOM from 'react-dom';
import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { Provider as ReduxProvider } from 'react-redux';
import store from './utils/redux-store';
import { exportToCss, OneDark } from './utils/themes';
import App from './components/app';
import * as css from './utils/css';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }

  #root {
    display: flex;
    min-height: 100vh;
  }
`;

const Viewport = styled.div`
  display: flex;
  flex-direction: column;
  font-family: sans-serif;
  flex-grow: 1;

  /* App-wide theme settings. */
  ${exportToCss(OneDark)}

  background-color: ${css.color('background')};
  color: ${css.color('text')};
`;

DOM.render(
  <React.StrictMode>
    <ReduxProvider store={store}>
      <Viewport>
        <GlobalStyle />
        <App />
      </Viewport>
    </ReduxProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
