import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import fetchMock from 'jest-fetch-mock';
import * as mocks from './mocks';

// Creates a mock `fetch(...)` API for all tests.
fetchMock.enableMocks();

Enzyme.configure({
  adapter: new Adapter(),
});

Object.defineProperties(globalThis, {
  // Required by uPlot.
  matchMedia: {
    value(query: string) {
      return new mocks.MockMediaQueryList(query);
    },
  },

  Notification: {
    value: Object.assign(
      jest.fn((title: string, options?: NotificationOptions) => {
        return new mocks.MockNotification(title, options);
      }),
      {
        requestPermission: jest.fn(),
        permission: 'default',
      },
    ),
  },

  ResizeObserver: {
    value: jest.fn((callback: ResizeObserverCallback) => {
      return new mocks.MockResizeObserver(callback);
    }),
  },
});
