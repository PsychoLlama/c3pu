export class MockMediaQueryList implements MediaQueryList {
  media: string;

  constructor(query: string) {
    this.media = query;
  }

  matches = false;
  onchange = null;

  addListener = jest.fn();
  removeListener = jest.fn();

  addEventListener = jest.fn();
  removeEventListener = jest.fn();
  dispatchEvent = jest.fn();
}

export class MockNotification implements Notification {
  static permission = 'default';

  constructor(title: string, options?: NotificationOptions) {
    this.title = title;
    Object.assign(this, options);
  }

  title = 'Excuse me';
  body = 'the oven is on fire';
  dir = 'ltr' as const;
  lang = 'en';
  icon = '';
  tag = '';
  data = null;

  onclick = null;
  onclose = null;
  onerror = null;
  onshow = null;

  close = jest.fn();
  addEventListener = jest.fn();
  removeEventListener = jest.fn();
  dispatchEvent = jest.fn();
}

export class MockResizeObserver implements ResizeObserver {
  callback: ResizeObserverCallback;

  constructor(callback: ResizeObserverCallback) {
    this.callback = callback;
  }

  observe = jest.fn();
  unobserve = jest.fn();
  disconnect = jest.fn();
}
