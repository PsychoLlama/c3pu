import { createSelector } from 'reselect';
import { State, Metric, DataPoint } from '../reducers/initial-state';

/**
 * Processes all data points into a list of incidents.
 *
 * Terminology:
 *
 *   Healthy: data is below the threshold.
 *   Unhealthy: data is above the threshold, but might not be a trend.
 *   Incident: data has exceeded the threshold for too long.
 *
 */
export const getIncidents = createSelector(
  [({ metric, state }: SelectorContext) => state.metrics[metric]],
  (metric: Metric) => {
    return bucketByThreshold(metric).reduce(
      (incidents: Array<Incident>, bucket: Bucket) => {
        const previousIncident = incidents[incidents.length - 1];
        const lastValueInBucket = bucket.values[bucket.values.length - 1];

        // Systems were healthy, but not for long. The incident continues.
        if (bucket.healthy === false && previousIncident?.resolved === false) {
          previousIncident.lastReported = lastValueInBucket.timestamp;
          return incidents;
        }

        // The last incident is far enough behind us to consider it resolved.
        if (previousIncident?.resolved === false && isHealed(metric, bucket)) {
          previousIncident.resolved = true;
          return incidents;
        }

        // This is the start of an incident.
        if (isIncident(metric, bucket)) {
          return incidents.concat({
            firstNoticed: bucket.values[0].timestamp,
            lastReported: lastValueInBucket.timestamp,
            resolved: false,
          });
        }

        // Nothing interesting happened.
        return incidents;
      },
      [],
    );
  },
);

/**
 * Bucket data points into consecutive groups of healthy vs unhealthy.
 * Sibling buckets of the same type are joined.
 *
 * Example: [Healthy, Unhealthy, Healthy]
 */
export const bucketByThreshold = createSelector(
  [
    (metric: Metric) => metric.dataPoints,
    (metric: Metric) => metric.monitoring.threshold.upperBound,
  ],
  (dataPoints: Array<DataPoint>, upperBound: number) => {
    return dataPoints.reduce((buckets: Array<Bucket>, point) => {
      const lastBucket: undefined | Bucket = buckets[buckets.length - 1];
      const healthy = point.value < upperBound;

      if (lastBucket?.healthy === healthy) {
        lastBucket.values.push(point);
      } else {
        buckets.push({ healthy, values: [point] });
      }

      return buckets;
    }, []);
  },
);

/**
 * Determine if the data points in the bucket were above the threshold for
 * long enough to constitute an incident.
 */
const isIncident = (metric: Metric, bucket: Bucket) => {
  const { timeToEscalation } = metric.monitoring;

  const firstDataPoint = bucket.values[0];
  const lastDataPoint = bucket.values[bucket.values.length - 1];
  const bucketPeriod = lastDataPoint.timestamp - firstDataPoint.timestamp;

  return !bucket.healthy && bucketPeriod >= timeToEscalation;
};

/**
 * Determines if enough time has passed since an incident to consider it
 * healed.
 */
const isHealed = (metric: Metric, bucket: Bucket) => {
  const { timeToCooldown } = metric.monitoring;

  const firstDataPoint = bucket.values[0];
  const lastDataPoint = bucket.values[bucket.values.length - 1];
  const bucketPeriod = lastDataPoint.timestamp - firstDataPoint.timestamp;

  return bucket.healthy && bucketPeriod >= timeToCooldown;
};

export interface Incident {
  /** Epoch timestamp */
  firstNoticed: number;

  /** Epoch timestamp */
  lastReported: number;

  /** Whether it later recovered */
  resolved: boolean;
}

interface Bucket {
  /** Whether values in this bucket are within the threshold. */
  healthy: boolean;
  values: Array<DataPoint>;
}

interface SelectorContext {
  metric: keyof State['metrics'];
  state: State;
}
