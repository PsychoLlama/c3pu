import { produce } from 'immer';
import { bucketByThreshold, getIncidents } from '../incidents';
import initialState, { State } from '../../reducers/initial-state';

describe('Incident selectors', () => {
  describe('bucketByThreshold', () => {
    function setup(patches = (() => undefined) as any) {
      const state = produce(initialState, patches);
      const buckets = bucketByThreshold(state.metrics.cpuLoad);

      return { state, buckets };
    }

    it('returns an empty list of buckets when no data exists', () => {
      const { buckets } = setup();

      expect(buckets).toEqual([]);
    });

    it('memoizes work against the data points', () => {
      const { state, buckets } = setup((state: State) => {
        state.metrics.cpuLoad.dataPoints.push({
          timestamp: 1,
          value: 10,
        });
      });

      expect(buckets).toEqual(bucketByThreshold(state.metrics.cpuLoad));
    });
  });

  describe('getIncidents', () => {
    function setup(patches = (() => undefined) as any) {
      const state = produce(initialState, patches);
      const incidents = getIncidents({ state, metric: 'cpuLoad' });

      return { state, incidents };
    }

    it('shows no incidents with healthy operating data', () => {
      const { incidents } = setup((state: State) => {
        const { upperBound } = state.metrics.cpuLoad.monitoring.threshold;

        state.metrics.cpuLoad.dataPoints.push(
          { timestamp: 1, value: upperBound - 1 },
          { timestamp: 2, value: upperBound - 2 },
        );
      });

      expect(incidents).toEqual([]);
    });

    it('produces incidents when unhealthy trends exceed escalation time', () => {
      const { incidents } = setup((state: State) => {
        const metric = state.metrics.cpuLoad;
        metric.monitoring.timeToEscalation = 100;
        metric.monitoring.threshold.upperBound = 10;
        metric.dataPoints.push(
          { timestamp: 123, value: 15 },
          { timestamp: 456, value: 15 },
        );
      });

      expect(incidents).toEqual([
        {
          firstNoticed: 123,
          lastReported: 456,
          resolved: false,
        },
      ]);
    });

    it('ignores healthy periods if they are brief enough', () => {
      const { incidents } = setup((state: State) => {
        const metric = state.metrics.cpuLoad;
        metric.monitoring.timeToEscalation = 100;
        metric.monitoring.timeToCooldown = 100;
        metric.monitoring.threshold.upperBound = 10;
        metric.dataPoints.push(
          { timestamp: 100, value: 15 },
          { timestamp: 200, value: 15 },
          { timestamp: 300, value: 5 },
          { timestamp: 400, value: 15 },
        );
      });

      expect(incidents).toEqual([
        {
          firstNoticed: 100,
          lastReported: 400,
          resolved: false,
        },
      ]);
    });

    it('marks incidents resolved after a reasonable healthy period', () => {
      const { incidents } = setup((state: State) => {
        const metric = state.metrics.cpuLoad;
        metric.monitoring.timeToEscalation = 100;
        metric.monitoring.timeToCooldown = 100;
        metric.monitoring.threshold.upperBound = 10;
        metric.dataPoints.push(
          { timestamp: 100, value: 15 },
          { timestamp: 200, value: 15 },
          { timestamp: 300, value: 5 },
          { timestamp: 400, value: 5 },
        );
      });

      expect(incidents).toEqual([
        {
          firstNoticed: 100,
          lastReported: 200,
          resolved: true,
        },
      ]);
    });

    it('correctly distinguishes series of incidents', () => {
      const { incidents } = setup((state: State) => {
        const metric = state.metrics.cpuLoad;
        metric.monitoring.timeToEscalation = 100;
        metric.monitoring.timeToCooldown = 100;
        metric.monitoring.threshold.upperBound = 10;
        metric.dataPoints.push(
          { timestamp: 100, value: 15 },
          { timestamp: 200, value: 15 },
          { timestamp: 300, value: 5 },
          { timestamp: 400, value: 5 },
          { timestamp: 500, value: 15 },
          { timestamp: 600, value: 15 },
          { timestamp: 700, value: 5 },
          { timestamp: 800, value: 15 },
        );
      });

      expect(incidents).toEqual([
        {
          firstNoticed: 100,
          lastReported: 200,
          resolved: true,
        },
        {
          firstNoticed: 500,
          lastReported: 800,
          resolved: false,
        },
      ]);
    });
  });
});
