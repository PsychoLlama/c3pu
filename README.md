# C3PU

A browser-based CPU load monitoring application.

## Running the App

Install the dependencies and start the development server:

- `yarn install`
- `yarn start`

Open the app in your browser: [localhost:8080](http://localhost:8080). The chart should slowly populate with data from the server.

## Requirements

> A user should be able to view your application to answer the following questions about their computer:
>
> - What is my computer's current average CPU load?
> - How did the average CPU load change over a 10 minute window?

The last 10 minutes of usage data, sampled every 10 seconds, is plotted on the dashboard's graph. The most recent point is your current average.

> - Has my computer been under heavy CPU load for 2 minutes or more? When? How many times?
> - Has my computer recovered from heavy CPU load? When? How many times?

Elevated CPU usage for 2 minutes creates a new unresolved incident in the incidents log. The user can see every incident over the last 10 minutes. When levels recover over a 2 minute period, the incident is marked resolved.

Each incident is tagged with the time it started and the time it ended.

> - The front-end application should alert the user to high CPU load.
> - The front-end application should alert the user when CPU load has recovered.
> - The alerting logic in your application should have tests.

Alerting is managed by the [Web Notifications API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API) so you don't miss an incident. (There's a prompt to allow notifications.)

An alert is sent whenever a new incident is created or resolved. Alerting logic and incident detection is covered under unit tests.

### Development Settings Panel

The default thresholds and sampling settings are fully configurable in the development panel. I found it useful for testing, hopefully you do too.

**Note:** Changing settings on existing data can retroactively create incidents. This is intentional.

The panel is hidden in production.

## Designing for Production

> Please write up a small explanation of how you would extend or improve your application design if you were building this for production.

**Disclaimer:** Naturally there are better places to implement a monitoring and alerting solution than a web browser. Of course, as a coding challenge, it requires some suspension of disbelief. I will try to keep improvements aligned with the spirit of the requirements.

---

The main challenge is deciding how to ship the server. I would advocate packaging the software as an Electron app, where the server runs headless in the main thread and registers controls with the system tray. There are several advantages:

- User flexibility: Users can easily see what apps are running, stop them, uninstall them, or start them automatically as a login item. They have complete control of the server.
- Easier access to the dashboard: Instead of connecting to localhost, we can show an Electron browser window. You don't need to know a port number.
- Deep integration with the host: Depending on the platform, this allows more reliable notifications.
- Cross-platform: This works on Linux, macOS, and Windows.

**Alternatively:** If all our users have Docker installed and feel comfortable in a terminal, we could ship a container that includes prebuilt web assets.

---

The second consideration is robustness. If this application is used for anything important, there are a few tasks we should consider before production:

- Add application error monitoring. We should know about critical failures in production.
- Add end-to-end tests covering alerts and incident reporting.
