# Reviewers: don't worry about this file. It manages the development
# environment (installed packages/versions). You've probably got these
# installed globally already.

{
  description = "Development Environment";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: {
      devShell = with nixpkgs.legacyPackages.${system};
        mkShell {
          nativeBuildInputs =
            [ nodejs-16_x (yarn.override { nodejs = nodejs-16_x; }) ];
        };
    });
}
